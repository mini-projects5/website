<?php                
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';
$name="";
$phone="";
$email="";
$message="";
$nameErr = "";
$phoneErr = "";
$emailErr = "";
$messageErr = "";
$success="";
$fail="";
$send= False;
function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

$mail = new PHPMailer(true);
$mail->CharSet  = 'UTF-8';
$autoemail = new PHPMailer(true);
$autoemail->CharSet  = 'UTF-8';
if(isset($_POST['submit'])){
    if (empty($_POST["name"])) {
        $nameErr = "Ime i prezime je obavezno!";
      } 
    else if((preg_match('/http|www/i',($_POST["name"]))) || (preg_match('~[0-9]+~',($_POST["name"])))) {
        $nameErr = "Ovo polje prihvaća samo ime i prezime!";
    }
    else {
        $name = test_input($_POST["name"]);
      }
    if (empty($_POST["phone"])) {
        $phoneErr = "Broj mobitela je obavezan!";
      } 
    else if(preg_match('/^[0-9]{10}+$/',($_POST["phone"])))  {
        $phone = test_input($_POST["phone"]);
    }
    else {
        $phoneErr = "Broj mobitela mora biti u valjanom formatu!";
    }
    if (empty($_POST["message"])) {
        $messageErr = "Poruka je obavezna!";
    } 
    else if((preg_match('/http|www/i',($_POST["message"])))) {
        $messageErr = "Poruka ne prihvaća URL i spam!";
    }
    else {
        $message = test_input($_POST["message"]);
    }
    if (empty($_POST["email"])) {
        $emailErr = "Email je obavezan!";
        
    } else {
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Email mora biti valjan!";
        } else {
            $email = test_input($_POST["email"]);
        }
        $email = test_input($_POST["email"]);
    }
    if(!empty($_POST['website'])) die();
    
    if ($nameErr =="" && $phoneErr == "" && $emailErr == ""  && $messageErr == "" && empty($_POST['website'])) {
        try {
            $mail->SMTPDebug = 0;                                   
            $mail->isSMTP();                                            
            $mail->Host       = 'smtp-mail.outlook.com;';                    
            $mail->SMTPAuth   = true;                             
            $mail->Username   = 'tonni-website@outlook.com';   
            $mail->Password   = 'toNNiWebsite';                         
            $mail->SMTPSecure = 'tls';                              
            $mail->Port       = 587;  
            
            $mail->isHTML(true);   
            
            $mail->From = ("tonni-website@outlook.com"); 
            $mail->FromName = ($_POST['name']); 
            $mail->AddAddress("tonni-website@outlook.com", "Website toNNi"); 
            $mail->AddAddress($_POST['email'], $_POST['name']); 
            $mail->AddReplyTo($_POST['email'], $_POST['name']);

            $mail->Subject = 'Poruka s web sjedišta toNNi';
            $mail->Body  = "Korisnik ". $_POST['name'] . " je poslao sljedeću poruku: <br/><br/>";
            $mail->Body  .= "<b>". $_POST['message'] . "</b>. <br/><br/>";
            $mail->Body  .= "Broj za kontakt je ". $_POST['phone'] . ".";

            $mail->AltBody  = "Korisnik ". $_POST['name'] . " je poslao sljedeću poruku: <br/><br/>";
            $mail->AltBody  .= "<b>". $_POST['message'] . "</b>. <br/><br/>";
            $mail->AltBody  .= "Broj za kontakt je ". $_POST['phone'] . ".";

            $mail->send();
            $send = True;

            $success = "Vaša poruka je uspješno poslana!";} 
            
        catch (Exception $e) {
            $fail= "Poruka nije mogla biti poslana! Molimo pokušajte kasnije.";
            $send = False;}
        }
    }

?>
<html lang="hr">

<head>
    <title>toNNi</title>

    <link rel="icon" id="faviconTag" type="image/svg" href="images/favicon.svg">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="toNNi je open source model za treniranje neuronskih mreža, razvijen u sklopu edukacije AI Centar Lipik">



    <link rel="shortcut icon" type="image/svg" href="images/favicon.svg" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300&family=Source+Serif+Pro:wght@500&display=swap"
        rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="preload" href="images/try.png" as="image">
    <link rel="preload" href="images/try-1600.png" as="image">

    <!-- <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css"> -->


</head>

<body>
    <div class="background">
        <!-- <img class="image-banner" src= "images/banner.png" alt="Banner naslovne stranice"> -->
        <div class="contain-nav">
            <nav class="navigation">
                <a class="nav-item" href=#tonni>toNNi</a><br>
                <a class="nav-item" href=#aicl>AI Centar Lipik</a><br>
                <a class="nav-item" href=#contact>kontakt</a>
            </nav>
        </div>
    </div>
    <div class="container-fluid content">
        <div class="contain-index">
            <div class="row red" id="tonni">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 image-n">
                    <img class="image-nn" src="images/new-logo.svg" alt="nn">
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 tekst-main">
                    <h1 class="o-nama">Što je toNNi?</h1>
                    <p data-aos="fade-up" data-aos-duration="2000" class="p-graf">toNNi je open source model za
                        treniranje neuronskih mreža. API za jednostavno korištenje. Čitka dokumentacija koja obuhvaća
                        sve aspekte neuronske mreže. Performanse usporedive sa mainstream bibliotekama za strojno
                        učenje. Razvijen je u Lipiku kao projekt u svrhu edukacije.
                    </p>
                </div>
            </div>
            <h1 class="features">Značajke</h1>
            <div data-aos="fade-up" data-aos-duration="1000" class="row red-features1">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 tekst-f txt-features-r">
                    <h3>Jednostavan API</h3>
                    <p class="paragraf">Neuronska mreža tonni je jednostavna fully-connected mreža koja služi za
                        treniranje modela za klasifikaciju i prepoznavanje objekata. API je jednostavan pa se je njime
                        lako služiti. Idealan je za pocetnike i one koje žele dublje shvatiti rad neuronskih mreža.
                    </p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 img-feature txt-features-l">
                    <img class="chess-pic" src="images/znacajke/prva.png">
                </div>
            </div>
            <div data-aos="fade-up" data-aos-duration="1000" class="row red-features1">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 order-md-last tekst-f txt-features-l">
                    <h3>Performanse</h3>
                    <p class="paragraf">Iako je naš model jednostavan te se ne koristi raznim metodama kojima se služe
                        komercijalne neuronske mreže, naši rezultati su usporedivi. Na svakom testnom datasetu naš model
                        je parirao mrežama kao tensorflow, keras te pytorch sa zanemarivim odstupanjem.
                    </p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 img-feature txt-features-r">
                    <img class="chess-pic" src="images/znacajke/perf.png">
                </div>
            </div>
            <div data-aos="fade-up" data-aos-duration="1000" class="row red-features1">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 tekst-f txt-features-r">
                    <h3>Dokumentacija</h3>
                    <p class="paragraf">Naša dokumentacija je temeljita i uvijek up to date. Osim opisa rada raznih
                        funkcija i metoda, sadrži i korisne informacije te objasnjenje pojedinih koraka.
                    </p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 img-feature txt-features-l">
                    <img class="chess-pic" src="images/znacajke/dokumentacija.png">
                </div>
            </div>
            <div data-aos="fade-up" data-aos-duration="1000" class="row red-features1">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 order-md-last tekst-f txt-features-l">
                    <h3>Layeri</h3>
                    <p class="paragraf">U svoj model možete dodavati više vrsta layera i aktivacijskih funkcija. Custom
                        parametri kao broj neurona , dense layeri za klasifikaciju , konvolucijski za rad sa slikama.
                        Mozete konstruirati vlastitu neuronsku mrezu.
                    </p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 img-feature txt-features-r">
                    <img class="chess-pic" src="images/znacajke/layer.png">
                </div>
            </div>
            <div data-aos="fade-up" data-aos-duration="1000" class="row red-features1">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 tekst-f txt-features-r">
                    <h3>Dodatne opcije</h3>
                    <p class="paragraf">Tokom treninga mreže moguce je upaliti razne postavke kao batch size, early
                        stopping te razni prikazi statistika. Pratimo predikcije i pratimo tocnost, a uskoro mozemo i
                        prikazati pomocu grafova.
                    </p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 img-feature txt-features-l">
                    <img class="chess-pic" src="images/znacajke/dodatno.png">
                </div>
            </div>
        </div>

        <div class="aicl">
            <div class="contain-index">
                <div class="row red-aicl" id="aicl">
                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 image-aicl">
                        <img class="image-aicl" src="images/aicl/glavna-glava.png" alt="nn">
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8 aicl-tekst">
                        <h2>AI Centar Lipik</h2>
                        <p class="paragraf">
                            Centar umjetne inteligencije Lipik (AI Center Lipik) objedinjuje Edukacijski centar umjetne
                            inteligencije koji
                            provodi edukacije i osposobljavanja polaznika te Startup inkubator i akcelerator umjetne
                            inteligencije koji pruža smještaj i mentorsku podršku tvrtkama koje se bave
                            programiranjem i umjetnom inteligencijom.
                        </p>
                        <p data-aos="fade-up" data-aos-duration="2000">
                            <a class="aicl-button" href="https://www.lipik.ai/">saznaj više</a>
                            <!-- <a href="mailto:aicenter@lipik.ai">kontakt</a>   -->
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contain-index">
            <div class="row red-contact">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 contact">
                    <h2 data-aos="fade-up" data-aos-duration="2000">Zanima te više? Javi nam se<h2>
                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>#contact"
                                enctype="multipart/form-data" id="contact">
                                <div class="form-inp">
                                    <input type="text" name="name" placeholder="Ime i prezime*" id="form34"
                                        class="form-control validate">
                                    <span class="error"> <?php echo $nameErr;?></span>
                                </div>
                                <div class="form-inp">
                                    <input type="email" name="email" placeholder="Email*" id="form29"
                                        class="form-control validate">
                                    <span class="error"> <?php echo $emailErr;?></span>
                                </div>
                                <div class="form-inp">
                                    <input type="text" name="phone" placeholder="Mobitel*" id="form32"
                                        class="form-control validate">
                                    <span class="error"> <?php echo $phoneErr;?></span>
                                </div>
                                <div class="md-form mb-5" id="website">
                                    <input name="website" type="text" id="website1" class="form-control"
                                        placeholder="Vaša web stranica" />
                                </div>
                                <div class="form-inp">
                                    <textarea type="text" name="message" placeholder="Poruka*" id="form8"
                                        class="md-textarea form-control" rows="4"></textarea>
                                    <span class="error"> <?php echo $messageErr;?></span>
                                </div> <br>


                                <button class="btn btn-send btn-unique" type="submit" name="submit" value="Submit"
                                    id="send">pošalji</button>
                                <div class="center-mess">
                                    <span class="fail"> <?php echo $fail;?></span>
                                    <span class="success"> <?php echo $success;?></span>
                                </div>

                            </form>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 frame">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2800.7920026455804!2d17.150345815751784!3d45.41353374484349!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4767730e148178e9%3A0x3e1e5255632ab353!2sAI%20Centar%20Lipik!5e0!3m2!1sen!2shr!4v1663605274041!5m2!1sen!2shr"
                        width="100%" style="border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="row red-footer">
            <div class="col-sm-12 col-md-4 col-lg-4 logo-footer">
                <a href="index.php"><img class="logo-white" src="images/logo-white.png"
                            alt="Ikona Facebook loga"></a>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 info">
                    Antonio Kezele<br>
                    <a href="mailto:akezele35@gmail.com">akezele35@gmail.com</a><br><br>
                    Powered by<br>
                    <a href="mailto:aicenter@lipik.ai">AI Centar Lipik</a><br><br>
                    Staklanska ul. 2 <br>
                    34551, Lipik
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 sm">
                    <a
                        href="https://gitlab.com/akezele35?fbclid=IwAR23XVj-EGAToXiA_N1zag5NQRqezHM9RWhCS-vZpsfEAq4A6lPN9wvLe24"><img
                            class="sm1" src="images/footer/gitlab-logo.png" alt="Ikona Facebook loga"></a>
                    <a href="https://www.facebook.com/nuklearni.kupus"><img class="sm2" src="images/footer/fb-logo.png"
                            alt="Ikona Facebook loga"></a>
                </div>
            </div>

            <!-- <div class="row red-footer-mobile">
            <div class="col-12 info-mobile">
                Antonio Kezele<br>
                <a href="mailto:akezele35@gmail.com">akezele35@gmail.com</a><br><br>
                Powered by<br>
                <a href="mailto:aicenter@lipik.ai">AI Centar Lipik</a><br><br>
                Staklanska ul. 2 <br>
                34551, Lipik
            </div>
            <div class="col-12sm">
                <a href="https://gitlab.com/akezele35?fbclid=IwAR23XVj-EGAToXiA_N1zag5NQRqezHM9RWhCS-vZpsfEAq4A6lPN9wvLe24"><img class="sm1"
                        src="images/footer/gitlab-logo.png" alt="Ikona Facebook loga"></a>
                <a href="https://www.facebook.com/nuklearni.kupus"><img class="sm2"
                        src="images/footer/fb-logo.png" alt="Ikona Facebook loga"></a>
            </div>
        </div> -->
        </div>
    </div>

</body>

</html>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
<script src="owlcarousel/owl.carousel.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>





<script>
AOS.init();
</script>

<script type="text/javascript" src="js/jScript.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    <?php if ($success != "" ||  $fail != "") : ?>
    document.getElementById('send').style.display = 'none';
    <?php endif; ?>
});
</script>
