//Brisanje podataka iz kontaktnog obrasca nakon osvježavanja prozora
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function(e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});
